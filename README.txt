**TL;DR:**
Extend the Metatag module to support relative or route-based fixed URLs.

**Problem with Metatag**
The wonderful Metatag module allows adding arbitrary <meta> tags with arbitrary values and Token support.

Sometimes, you may want to enter a fixed URL value that is not derived from the page context (commonly in Metatag's default/"Global" tags), so Tokens don't help and you're stuck entering a relative or absolute URI.

*Problem Example*

Assume your custom theme provides a 60x60px Apple touch icon; it's not in public files, it's in the theme's images directory. You want to reference it across all pages of your site, so you set Metatag: Apple touch icon: 60px x 60px to "themes/custom/mytheme/images/apple60.png". This works great on the homepage of example.com, but because it's a relative URL, when you go to example.com/foo/bar, the effective URL is "example.com/foo/themes/custom/mytheme/images/apple60.png," which fails to load.

Bad solution: Know that your site's root is "/" and prefix that to your path: "/themes/custom/mytheme/images/apple60.png." This root-relative path will work, unless your root changes (on a development box, or if you move to a subdirectory install, etc.).

Good solution: Tell Metatag that the URI you're adding is relative, and have it run the URI through Url::fromUri() to output a proper URL relative to the Drupal root (whatever that is or may be in the future).

**Solution provided by this module**
This module adds processing for these functions in Metatag values:

- urlFromUri(...)
- urlFromRoute(...)

Simply wrap a function around your value, and the appropriate Url method will be invoked and returned. For example, "urlFromUri(base:themes/custom/mytheme/images/apple60.png)" will pass "base:themes/custom/mytheme/images/apple60.png" to Url::fromUri() and return the proper Drupal-base-relative URL.

Note: Url::fromUri() requires full URIs including the scheme. Our example uses "base:" which means relative to the Drupal root.
